# IronMan
This is a Scrapy project to scrape ads from https://hdlava.me/

## Pre-requirements
```
    pip install -r ironman/requirements.txt
```

* [ChromeDriver] (http://chromedriver.chromium.org)

## Extracted data

The extracted data looks like this sample:

```
title   text    image
3Д онлайн игра! MMORPG игра которая захватывает дух. Легкая прокачка героя.     https://google.com
Поиграй с ней!  Эта MMORPG затягивает с первых минут. 100% бесплатная игра.     https://google.com

```


## Spiders

This project contains one spider and you can list him using the `list`
command:

    $ scrapy list
    ironman


## Running the spiders

You can run a spider using the `scrapy crawl` command, such as:

    $ scrapy crawl ironman

If you want to save the scraped data to a file, you can pass the `-o` option:
    
    $ scrapy crawl ironman -o ironman.tsv

