import scrapy
from selenium import webdriver


class IronManSpider(scrapy.Spider):
    name = 'ironman'
    start_urls = ['https://hdlava.me/film/zheleznyy-chelovek.html']
    ad_group_xpaths = [
        '/html/body/main/div[2]/div[2]/div[4]/div[2]/div/div/div/table/tbody/tr/td',
        '/html/body/main/div[2]/div[3]/div[4]/div[1]/div/div/table/tbody/tr/td',
    ]

    def __init__(self):
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(6)

    def parse(self, response):
        self.driver.get(response.url)
        for ads_group_xpath in self.ad_group_xpaths:
            ad_group = self.driver.find_elements_by_xpath(ads_group_xpath)
            for ad in ad_group:
                item = {}
                item['title'] = ad.find_element_by_xpath('.//div[contains(@class, "-title")]').text
                item['text'] = ad.find_element_by_xpath('.//div[contains(@class, "-desc")]').text
                item['image'] = ad.find_element_by_xpath('.//img[contains(@class, "-img")]').get_attribute('src'),
                yield item
